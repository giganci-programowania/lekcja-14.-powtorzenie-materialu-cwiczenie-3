﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lekcja_14._powtorzenie_materialu_cw_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Podaj swój wiek");
            int wiek = int.Parse(Console.ReadLine());

            Console.WriteLine("Podaj ile masz pieniędzy");
            int kasa = int.Parse(Console.ReadLine());

            SprawdzCzyMoznaWejsc(wiek, kasa);

            Console.ReadLine();
        }

        static void SprawdzCzyMoznaWejsc(int wiek, int kasa)
        {
            if (wiek >= 16 && kasa >= 20)
            {
                Console.WriteLine("Zapraszamy na seans!");
            }
            else if (wiek < 16 && kasa < 20)
            {
                Console.WriteLine("Masz za mało kasy i jesteś za młody!");
            }
            else if (wiek < 16)
            {
                Console.WriteLine("Jesteś za młody żeby wejść!");
            }
            else if (wiek < 20)
            {
                Console.WriteLine("Masz za mało kasy aby wejść!");
            }
        }
    }
}
